/*
 * TODO
 */

/* [base plate] */
// base plate height (CNC Z)
BH = 40;
// base plate length (CNC Y)
BL = 30;
// base plate hole distance vertical
BHDV = 20;
// base plate strength (CNC X)
BW = 3;
// base plate hole diameter
BHD = 3.1;
// (see https://amesweb.info/Screws/Metric-Clearance-Hole-Chart.aspx)
// X-Baseplate extra length
BXEL = 5;
// diameter of X mounting holes
BXHD = 5;

/* [micro switch] */
// e.g. https://sten-eswitch-13110800-production.s3.amazonaws.com/system/asset/product_line/data_sheet/125/SS.pdf
// height (CNC Z)
MSH = 12.7;
// length, excl contacts (CNC Y)
MSL = 6.6;
// width (CNC X)
MSW = 5.8;
// hole diameter
MSHD = 2;
// holder strength
MSS = 2;
// bit of vertical distance, so it doesn't sit so snug
d = 0.1;
// how far in should the X switches be?
MSXX = 2;

/* [arm] */
// arm base width (CNC Y)
ABW = 12;
// arm base length (CNC X)
ABL = 32;
// arm base strength (CNC Z)
ABS = 1.6;
// flange width (CNC Y)
AFW = 8;
// flange depth (CNC Z)
AFD = 3;
// hole distance to table edge
AHX = 12;
// hole diameter
AHD = 6;
// arm width (CNC X)
//   will be same as micro-switch width //
// arm strength (CNC Y)
AS = 2;
// arm length (CNC Z)
AL = 12;

/* [machine dimensions] */
// width of left plate (Y)
LPW = 60;
// thickness of left plate (X)
LPT = 10;
// distance between upper rod and upper edge
URZ = 14;
// distance between upper rod and back
URY = 38;
// distance between upper edge of "glass" and upper edge
GD = 9;
// sled width (X)
SW = 58;
// inner width of machine (X)
MIW = 360;
// machine inner length (Y)
MIL = 290;
// Y-distance between back inner edge and left holding plate back edge
PYD = 42;
// height of the holding plate (from bottom to top)
PH = 220;

/* [sled] */
// x-position of left edge of sled (just for the ghost)
SX = 10; // [2:300]
// sledge depth
SD = 66;
// distance between back edge of sled and center of rod
SRYD = 14;
// distance between top of sled and center of rod
SRZD = 14;
// depth of back of sled
SBD = 28;
// total depth of sled
STD = 67;
// depth of lower sled plate
SLD = 52;
// height of sled
SH = 100;
// sled upper/lower plate thickness (Z)
SPZ = 10;
// stepper width
STW = 42;
// stepper height
STH = 33;
// stepper pole height
SPH = 14;
// stepper pole diameter
SPD = 7;
// stepper pole center distance
SPCD = 31;
// distance between stepper pole and front edge of sled
SPYD = 2;
// spindle holder height
SHH = 34;
// spindle holder depth
SHD = 79;
// total depth of spindle holder + sled
SSHD = 116;
// spindle motor diameter
SMD = 44;
// distance between spindle motor and front edge of spindle holder
SMYD = 4;
// width of the small indentation of the top plate
SIW = 16;
// depth of the small indentation of the top plate
SID = 2;
// sled Z (just for the ghost)
SZ = -3; // [-45:0]
// distance between Z rods
ZRD = 38;
// how far in should the Z switch be?
MSZZ = 1;

/* [table] */
// guiding rod diamater
GRD = 10;
// CNC table width (X)
CTW = 300;
// CNC table length (Y)
CTL = 180;
// CNC table thickness (Z)
CTT = 12;
// y-position of table (just for the ghost)
CTY = -2;   // [-30:140]
// distance between Y rods
YRD = 150;

/* [emergency stop] */
// hole diameter
ESHD = 16;
// flat side diameter
ESFD = 15;
// length into case (just for modeling)
ESLI = 21;
// body diameter (just for modeling)
ESBD = 23;
// body lenth (case to top) (just for modeling)
ESBL = 19;
// red button diameter (just for modeling)
ESRD = 32;
// red button height (just for modeling)
ESRH = 9;

/* [camera] */
// Y distance to drill
CYD = 40;
// height of corner above plate
CZD = 10;
// rotate camera down by angle
CRD = 54; // [20:70]
// rotate camera left by angle
CRL = 20; // [10:50]

/* [render] */
// what to render
render = "ALL";  // ["ALL", "xend", "yend", "zend", "arm", "camera", "machine"]
// layer height
LH = 0.2;
// epsilon
e = 0.02;
// taper width
TW = 3;

$fa = 5;
$fs = 0.3;

// distance between table and right extrusion (CNC X)
TD = (MIW-2*20-CTW)/2;

// Camera Y position relative to endstop_x origin
// this can be simplified, but I don't care
cyp = -(MIL-PYD-20) + MIL-PYD-URY+SRYD-STD + SPYD+SPD/2-(STW-SPCD)/2 + STD-SSHD + SMYD+SMD/2 -CYD;

module switch() {
    %scale(0.98) T(+3, -3.1, -6.5) rotate([90, -90, 0]) import("Micro_limit_switch.stl");
}

module T(x=0,y=0,z=0) {
    translate([x,y,z]) children();
}
module Tx(x=0) {
    T(x=x) children();
}
module Ty(y=0) {
    T(y=y) children();
}
module Tz(z=0) {
    T(z=z) children();
}

module rounded_plate(x=0, y=0, z=0, r=2) {
    linear_extrude(z) {
        hull() {
            for (i=[-1,1]) for (j=[-1,1])
                translate([x/2+i*(x/2-r), y/2+j*(y/2-r)]) circle(r=r);
        }
    }
}

module endstop_x(estop=false) {
    // planned position of micro-switch
    T(MSXX, 20-(LPW+BXEL+d+MSS), 20-GD+MSS) rotate([0,-90, -90]) switch();

    // width (X/Y) of estop holder
    // distance between back/front walls and screws for extrusion/upper rod should be the same
    esw = URY + 10;
    // distance of X-axis hole to top
    xaz = URZ + (SH-2*SRZD)/2;
    // height (Z) of estop holder
    // should reach the screw for the X axis
    esh = xaz + (BXHD+6)/2 + 4;

    difference() {
        bpl = LPW + MSH+2*d+2*MSS + TW + BXEL;
        union() {
            // base plate
            T(-LPT-BW, 20-bpl, 20-24) rotate([90,0,90]) cube([bpl, 24, BW]);
            if (estop) {
                // vertical plate for estop holder
                T(-LPT-BW, 20-esw, 20-esh) cube([BW, esw, esh]);
                // top plate
                difference() {
                    T(-LPT-esw, 20-esw, 20-BW) cube([esw, esw, BW]);
                    T(-LPT-esw/2, 20-esw/2, 20+e) #estop();
                }
                // front/back wall
                for (y=[20,20-esw+BW])
                hull() {
                    T(-LPT-esw, y-BW, 20-BW) cube([esw, BW, e]);
                    T(-LPT-BW,  y-BW, 20-esh) cube([e, BW, esh]);
                }

                // The estop-side also has the camera
                difference() {
                    hull() {
                        T(-LPT-BW, 20-bpl, 20-24) #cube([BW, e, 24]);
                        T(-LPT, cyp, 20-24) cylinder(d=8, h=10+CZD);
                    }
                    T(-LPT, cyp, 20-24+2) #cylinder(d=5.2, h=10+CZD);
                }
            }
            // bottom of holder
            T(-LPT, 20-(LPW+BXEL+MSH+2*d+2*MSS), 20-GD) cube([LPT+MSXX, MSH+2*d+2*MSS, MSS]);
            // back rest
            T(-LPT, 20-(LPW+BXEL+MSH+2*d+2*MSS), 20-GD+MSS) cube([LPT+MSXX-(MSL+d), MSH+2*d+2*MSS, MSW/2-0.5]);
            // back side
            T(-LPT, 20-(LPW+BXEL+MSS),           20-GD+MSS) cube([LPT+MSXX, MSS, MSW]);
            // front side
            T(-LPT, 20-(LPW+BXEL+MSH+2*d+2*MSS), 20-GD+MSS) cube([LPT+MSXX, MSS, MSW]);
            // taper
            hull() {
                T(-LPT-e, 20-bpl, 20-GD-TW) cube([e, MSH+2*d+2*MSS + 2*TW, MSS+MSW+2*TW]);
                T(-LPT+TW-e, 20-(LPW+BXEL+MSH+2*d+2*MSS), 20-GD) cube([e, MSH+2*d+2*MSS, MSS+MSW]);
            }
            // for the holes
            for (y=[3.15, 3.15+6.5])
                T(MSXX-5.1, 20-(LPW+BXEL+MSS+d+y), 20-GD+MSS) cylinder(d=MSHD, h=MSW);
        }
        // make sure nothing (e.g. the taper) doesn't go over the edge
        T(-LPT-BW-e, 20-bpl-e, 20) cube([BW+LPT+MSXX+2*e, bpl+2*e, 10]);
        // access to contacts
        T(-LPT-BW-e, 20-(LPW+BXEL+MSH+2*d+MSS), 20-GD+MSS) cube([BW+TW+2*e, MSH+2*d, MSW+e]);
        T(-LPT,      20-(LPW+BXEL+MSH+2*d+MSS), 20-GD+MSS) cube([   TW+e,   MSH+2*d, MSW+TW+e]);
        // mounting holes
        T(-LPT-BW-e, 10, 10) rotate([0,90,0]) cylinder(d=BXHD, h=BW+2*e);
        T(-LPT-BW-e, 20-URY, 20-URZ) rotate([0,90,0]) cylinder(d=BXHD, h=BW+2*e);
        T(-LPT-BW-e, 20-URY, 20-xaz) rotate([0,90,0]) cylinder(d=BXHD, h=BW+2*e);
        // workaround for too short screws
        T(-LPT-BW-e, 10, 10) rotate([0,90,0]) cylinder(d=BXHD+6, h=BW-1+e);
        T(-LPT-BW-e, 20-URY, 20-URZ) rotate([0,90,0]) cylinder(d=BXHD+6, h=BW-1+e);
        T(-LPT-BW-e, 20-URY, 20-xaz) rotate([0,90,0]) cylinder(d=BXHD+6, h=BW-1+e);

    }
}


module camera() {
    // Same origin as endstop_x

    cl=44.1;
    cw=31;

    T(-LPT, cyp, 20-GD+CZD) {
        rotate([0,CRD,CRL]) rotate([180,90,0]) union() {
            // ghost of camera housing
            %render() T(-16, 27.1, -32) intersection() {
                import("ESP32_CAM_V2.stl");
                T(-15.2,-28,0) cube([32, 46, 33]);
            }
            // backplate of camera, with hole for cable
            difference() {
                union() {
                    linear_extrude(BW) {
                        hull() {
                            for (x=[-cw+3:-3]) for (y=[3:cl-3]) translate([x,y]) circle(r=3);
                            translate([-2,0.5]) circle(r=3);
                        }
                    }
                    // holders of backplate
                    for (y=[2,cl-5]) T(-cw+2, y, -12) cube([cw-4, 3, 12]);
                }
                T(-6, cl-13, -20) cube([8, 9, 20+BW+e]);
            }
            // press down camera
            // T(-(cw+10)/2, 3, -(floor(1.8/LH)*LH)) cube([10, cl-6, LH]);
            T(-(cw+10)/2, (cl-10)/2, -1.4) cube([10,10,1.4]);
            // viewing area
            // %color([0.8, 0.8, 0.4, 0.6]) T(-cw/2, cl-12, -25-vl) cylinder(h=vl, r1=vl*sin(30), r2=1);
        }
        // cylinder to attach camera to holder
        difference() {
            Tz(-20) cylinder(d=5, h=30);
            rotate([0,CRD,CRL]) rotate([180,90,0]) {
                Tz(5+BW) cube([20,20,10], center=true);
                T(-3,3,-10) cylinder(r=3, h=10);
            }
        }
    }

    // mark the area covered by the camera
    vl = 500;
    %color([0.8,0.8,0.5,0.5]) intersection() {
        T(-LPT, cyp, 20-GD+CZD) rotate([0,CRD,CRL]) rotate([180,90,0]) T(-cw/2, cl-12, -25-vl) cylinder(h=vl, r1=vl*sin(30), r2=1);
        T(0, -MIL, -PH+20+40+CTT-0.5) cube([MIW, 2*MIL, 0.5+e]);

    }

}


module sled() {
    Tx(SW) rotate([90,0,-90]) linear_extrude(SW) {
        difference() {
            union() {
                hull() {
                    difference() {
                        // rounded corners
                        union() {
                            translate([-STD+SRZD, SPZ-SRZD]) circle(r=SRZD);
                            translate([-STD+SRZD, SPZ-SH+SRZD]) circle(r=SRZD);
                        }
                        translate([-STD+SRYD, SPZ-SH-e]) square([SRZD+e, SH+2*e]);
                    }
                    translate([-STD+SBD-e, SPZ-SH]) square([e, SH]);
                }
                // upper plate
                translate([-STD+SBD, 0]) square([STD-SBD, SPZ]);
                // lower plate
                translate([-STD+SBD, SPZ-SH]) square([SLD-SBD, SPZ]);
            }
            // rods
            translate([-STD+SRYD, SPZ-   SRZD]) circle(d=GRD, $fn=16);
            translate([-STD+SRYD, SPZ-SH+SRZD]) circle(d=GRD, $fn=16);
            translate([-STD+SRYD, SPZ-SH/2])    circle(d=8, $fn=16);
        }
    }

    // stepper poles
    for (x=[-SPCD/2, +SPCD/2]) for (y=[0, SPCD])
        T(SW/2+x, SPYD+SPD/2+y, SPZ) cylinder(d=SPD, h=SPH);
    // stepper
    T((SW-STW)/2, SPYD+SPD/2-(STW-SPCD)/2, SPZ+SPH) cube([STW, STW, STH]);
    // spindle holder
    T(0, STD-SSHD, -SHH+SZ) linear_extrude(SHH) {
        difference() {
            hull() {
                translate([0,17]) square([SW, SHD-17]);
                translate([17,0]) square([SW-2*17, e]);
            }
            translate([SW/2, SMYD+SMD/2]) circle(d=SMD);
        }
    }
    // Z rail
    T(SW/2, SPYD+SPD/2+SPCD/2, SPZ-SH)       cylinder(d=  8, h=SH+SPH, $fn=16);
    // Z rods
    T((SW-ZRD)/2, SPYD+SPD/2+SPCD/2, SPZ-SH) cylinder(d=GRD, h=SH, $fn=16);
    T((SW+ZRD)/2, SPYD+SPD/2+SPCD/2, SPZ-SH) cylinder(d=GRD, h=SH, $fn=16);
}
// !sled();

module machine() {
    // base frame
    T(     0,   0, -40) cube([ 20, MIL, 40]);
    T(MIW-20,   0, -40) cube([ 20, MIL, 40]);
    T(     0, -10, -40) cube([MIW, 10, 40]);
    T(     0, MIL, -40) cube([MIW, 10, 40]);
    // upper extrusion
    T(0, MIL-PYD-20, PH-40-20) cube([MIW, 20, 20]);
    // main body of sled
    T(SX, MIL-PYD-URY+SRYD-STD, PH-SPZ-40) sled();
    // X rods
    T(0, MIL-PYD-URY, PH-40-URZ) rotate([0,90,0]) cylinder(d=GRD, h=MIW, $fn=16);
    T(0, MIL-PYD-URY, PH-40-SH+SRZD) rotate([0,90,0]) cylinder(d=GRD, h=MIW, $fn=16);
    T(0, MIL-PYD-URY, PH-40-SH/2) rotate([0,90,0]) cylinder(d=8, h=MIW, $fn=16);
    // left/right plate
    for (x=[0, MIW+LPT])
        T(x, MIL-PYD, -40) rotate([90,0,-90]) linear_extrude(LPT) {
            difference() {
                square([LPW, PH]);
                translate([LPW-10, PH-10]) square(10);
                // screw hole for upper X rod
                translate([URY, PH-URZ]) circle(d=5);
                // screw hole for X axis
                translate([URY, PH-URZ-(SH-2*SRZD)/2]) circle(d=5);
                // screw hole for top extrusion
                translate([10, PH-10]) circle(d=5);
            }
            translate([50, PH-10]) circle(r=10);
        }
    // glass
    for (x=[0, MIW+5]) T(x, MIL-PYD-LPW, -40) {
        ly = MIL-PYD-LPW+10;
        lz = PH-GD;
        rotate([90,0,-90]) linear_extrude(5) {
            difference() {
                square([ly, lz]);
                translate([60,60+(lz-ly)]) square([ly-60, ly-60]);
            }
            intersection() {
                square([ly, lz]);
                translate([60,60+(lz-ly)]) circle(r=ly-60);
            }
        }
    }
    // table
    T(20+TD, CTY, 0) cube([CTW, CTL, CTT]);
    // Y rods
    T((MIW-YRD)/2, 0, -20) rotate([-90,0,0]) cylinder(d=GRD, h=MIL, $fn=16);
    T((MIW+YRD)/2, 0, -20) rotate([-90,0,0]) cylinder(d=GRD, h=MIL, $fn=16);
    T((MIW    )/2, 0, -20) rotate([-90,0,0]) cylinder(d=  8, h=MIL, $fn=16);
}

module endstop_y() {
    rotate([0,0,180]) {
        // base plate
        difference(){
            // Tz(-BH) cube([BW, BL, BH]);
            rotate([0,90,0]) rounded_plate(BH, BL, BW, (BL-MSL-MSS)/2);
            yd = (BH-BHDV)/2;
            for (i=[5,BL-5]) for (j=[-yd, -BH+yd])
                T(-e, i, j) rotate([0,90,0]) #cylinder(d=BHD, h=BW+2*e);
        }

        // holder
        T(0, BL/2+(MSL+2)/2, -4) {
            // ghost example switch
            T(TD, 0, -(MSS+d) ) switch();

            // base (to reach to table)
            T(BW, -(MSL+d+MSS), -(2*MSS+2*d+MSH)) cube([TD-BW, MSS+d+MSL, (2*MSS+2*d+MSH)]);

            // for the holes
            for (z=[3.15, 3.15+6.5])
                T(TD, -5.1, -(MSS+d+z)) rotate([0,90,0]) cylinder(d=MSHD, h=MSW-e);
            // upper/lower flat
            for (z=[0, MSH+MSS+2*d])
                T(TD, -(MSL+d+MSS), -(MSS+z))   cube([MSW, MSL+d+MSS, MSS]);
            // back rest
            T(TD, -(MSL+d+MSS), -(MSS+2*d+MSH)) cube([MSW/2-0.5, MSS, MSH+2*d]);

            //taper
            hull() {
                T(BW-e, -(MSL+d+MSS+TW), -(2*MSS+2*d+MSH+TW)) cube([e, MSS+d+MSL+2*TW, (2*MSS+2*d+MSH+2*TW)]);
                T(BW+TW-e, -(MSL+d+MSS), -(2*MSS+2*d+MSH)) cube([e, MSS+d+MSL, (2*MSS+2*d+MSH)]);
            }
        }
    }
}

module arm_y() {
    rotate([0,180,180]) {
        difference() {
            union() {
                Tx(-ABL) rounded_plate(ABL, ABW, ABS, r=AFW/2);
                Tx(-(ABL-10)) cube([ABL-10, ABW, ABS]);
                T(-ABL, (ABW-AFW)/2, -AFD) rounded_plate(ABL, AFW, AFD, r=AFW/2);
                T(-(ABL-10), (ABW-AFW)/2, -AFD) cube([ABL-10, AFW, AFD]);
            }
            T(-ABL/2, ABW/2, -AFD-e) cylinder(d=AHD, h=ABS+AFD+2*e);
        }
        T(-MSW, (ABW-AS)/2, 0) cube([MSW, AS, AL]);
    }
}

module endstop_z() {
    // planned switch position
    T(MSS, -(MSS+d), -SPZ-MSZZ) rotate([-90,0,0]) switch();

    difference() {
        union() {
            // base plate
            cube([SW, SPYD+SPD+SPYD, BW]);
            // plate between poles, for better printing
            T((SW-SPCD)/2, SPYD-2, SPH-BW) cube([SPCD, SPD+4, BW]);
            // stepper poles
            for (x=[-SPCD/2, +SPCD/2])
                T(SW/2+x, SPYD+SPD/2, 0) cylinder(d=SPD+4, h=SPH);
        }
        for (x=[-SPCD/2, +SPCD/2])
           T(SW/2+x, SPYD+SPD/2, -e) cylinder(d=SPD+0.2, h=SPH+2*e);
        // stepper has a small cylindrical plate, need to cut it out
        T(SW/2, SPYD+SPD/2+SPCD/2, BW+e) cylinder(d=23, h=SPH);
        // cutout, so this can be snapped on
        #T((SW-SPCD)/2-SPD/2+0.2, SPYD+SPD/2, -e) cube([SPD-0.4, 10, SPH+2*e]);
        #T((SW+SPCD)/2-SPD/2+0.2, SPYD+SPD/2, -e) cube([SPD-0.4, 10, SPH+2*e]);
    }

    difference() {
        T(0, -MSS, -SPZ-MSZZ) cube([SW, MSS, MSZZ+SPZ+BW]);
        T((SW-SIW)/2-SID+MSS*(sqrt(2)-1), -MSS-e, -SPZ-MSZZ-e) cube([SIW+2*SID-2*MSS*(sqrt(2)-1), MSS+e, MSZZ+SPZ+BW+2*e]);
    }
    // indentation
    T((SW-SIW)/2, -MSS+SID, -SPZ-MSZZ) cube([SIW, MSS, MSZZ+SPZ+BW]);
    T((SW-SIW)/2-SID, 0, -SPZ-MSZZ) rotate([0,0,-45]) Ty(-MSS*(sqrt(2)-1))
        cube([MSS, MSS*(sqrt(2)-1)+SID*sqrt(2), MSZZ+SPZ+BW]);
    T((SW+SIW)/2+SID, 0, -SPZ-MSZZ) rotate([0,0,-135]) Ty(-SID*sqrt(2))
        cube([MSS, MSS*(sqrt(2)-1)+SID*sqrt(2), MSZZ+SPZ+BW]);

    T(0,   -(MSH+2*MSS+2*d), -SPZ-MSZZ) cube([MSS, MSH+MSS+2*d, MSZZ+SPZ+BW]);
    T(MSS, -(MSH+2*MSS+2*d), -SPZ-MSZZ) cube([MSW, MSS,         MSZZ+SPZ+BW]);
    // back rest
    T(MSS, -(MSH+MSS+2*d), -SPZ-MSZZ+MSL+d) cube([MSW/2-0.5, MSH+2*d, MSZZ+SPZ+BW-MSL-d]);
    // for the holes
    for (y=[3.15, 3.15+6.5])
        T(MSS, -(MSS+d+y), -SPZ-MSZZ+5.1) rotate([0,90,0]) cylinder(d=MSHD, h=MSW);
    // taper
    T(MSS+(MSW/2-0.5), -MSS, 0) rotate([0,0,45]) T(-MSS/2,-MSS/2,0) cube([MSS,MSS,BW]);
    T(MSS+(MSW/2-0.5), -(MSS+2*d+MSH), 0) rotate([0,0,45]) T(-MSS/2,-MSS/2,0) cube([MSS,MSS,BW]);
}

module estop() {
    intersection() {
        Tz(-ESLI-e)   cylinder(d=ESHD, h=ESLI+2*e);
        T(-ESHD/2, -ESFD/2, -ESLI-e) cube([ESHD, ESFD, ESLI+2*e]);
    }
    cylinder(d=ESBD, h=ESBL-ESRH+e);
    Tz(ESBL-ESRH) cylinder(d=ESRD, h=ESRH);
}

if (render == "yend") {
    endstop_y();
    T(-(MIW-20), -(MIL-60), 0) %machine();
}

if (render == "xend") {
    endstop_x(estop=true);
    T(0, -(MIL-PYD-20), -(PH-40-20)) %machine();
    %camera();
}

if (render == "zend") {
    endstop_z();
    T(-SX, -(MIL-PYD-URY+SRYD-STD), -(PH-40)) %machine();
}

if (render == "arm") {
    arm_y();
    T(-(MIW-20-TD), -MIL/2, 0) %machine();
}

if (render == "camera") {
    camera();
}

if (render == "machine") {
    machine();
}

if (render == "ALL") {
    %machine();
    T(MIW-20, MIL-40, 0) endstop_y();
    T(MIW-20,     40, 0) mirror([0,1,0]) endstop_y();
    T(MIW-20-TD, CTY+CTL/2+ABW/2, 0) arm_y();
    T(0, MIL-PYD-20, PH-40-20)  endstop_x(estop=true);
    T(0, MIL-PYD-20, PH-40-20) camera();
    T(MIW, MIL-PYD-20, PH-40-20) mirror([1,0,0]) endstop_x(estop=false);
    T(SX, MIL-PYD-URY+SRYD-STD, PH-40) endstop_z();
}

