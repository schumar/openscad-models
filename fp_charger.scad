// battery length (total)
bl = 66.8;
// battery width
bw = 44;
// battery height
bh = 6;

// gnd-contact distance to edge
cgd = 6;

// pogo pin diameter
ppd = 1;
// pogo pin length (pressed)
ppl = 13;

// wall width
w = 1.6;

// epsilon
e = 0.01;

// plus-contact distance to edge
cpd = cgd + 3 * 2.54;

// corner length
cl = cgd + w;

/*
  ╭┈┈┈┈┈ ━  ━ ╮
  ┏━━  ━━╫━━╫━┓     ── y=0
  ┃           ┃
  ┊           ┊
  ┊           ┊
  ┊           ┊
  ┊           ┊
  ┊           ┊
  ┃           ┃
  ┗━━       ━━┛
  │
  x=0

*/

// base plate
translate([0, 0,-w]) linear_extrude(w) difference() {
    polygon([
            [-w, -bl-w],
            [-w, w],
            [bw-cpd-w, ppl+w],
            [bw-cgd+w, ppl+w],
            [bw+w, w],
            [bw+w, -bl-w]
    ]);
    polygon([
            [cl, -bl+cl],
            [cl, 0],
            [bw-cgd+w, 0],
            [bw-cgd+w, -bl+cl]
    ]);
}

// corner (2D), L-shape, origin in inside corner
module corner() {

    polygon([
            [-w, -w],
            [-w, cl],
            [ 0, cl],
            [ 0,  0],
            [cl,  0],
            [cl, -w]
    ]);
}

module holder(){
    translate([0,0,-e]) linear_extrude(bh+e) {

        // bottom left corner
        translate([0, -bl]) corner();
        // bottom right corner
        translate([bw, -bl]) mirror([1,0]) corner();
        // top left corner
        mirror([0,1]) corner();
        // top right corner
        translate([bw, 0]) mirror([1,1]) corner();

        // pogo pin stoppers
        translate([bw - cpd -w, ppl]) square([2*w+(cpd-cgd), w]);
        // pogo hole holder
        translate([bw - cpd -w, 0]) square([2*w+(cpd-cgd), 2*w]);
    }
}

difference() {
    holder();
    for(x=[cgd, cpd])
        #translate([bw-x, -e/2, bh/2-.1]) rotate([-90,0,0]) cylinder(d=ppd, h=2*w+e, $fn=6);
}
