// clip width
cw = 20;

// clip length (over glass)
cl = 10;

// glass width
gw = 6.7;

// clampdown
cd = 0.5;

// clip wall strength
ct = 3;

// height of endoscope
h = 15;

// diameter of endoscope
d = 7.4;

// length of endoscope
l = 30;

// off center
oc = 70;

// focus height
fh = 20;

// bed depth
bed = 230;

// Minimum fragment size
$fs = 0.4;
// Minimum fragment angle
$fa = 6;

bedh = bed / 2;

rotate([90, -atan(oc/bedh)-30, 0]) {
difference() {
    translate ([-cw/2,   -ct, -gw-ct]) cube ([cw, cl+ct, gw + 2*ct]);
    #translate ([-cw/2-1, 0, -gw])
    rotate([90,0,90]) linear_extrude(cw+2) polygon ([[0,0], [cl+0.1,cd], [cl+0.1,gw-cd], [0,gw]],
            [[0,1,2,3]]);
}

translate([-3,-ct]) cube([6, 6, h-d/2-1.0]);

translate([0, 0, h])
rotate([-atan((h-fh)/sqrt(bedh*bedh + oc*oc)), 0, -atan(oc/bedh)])
translate([0,-d,0])
    difference() {
        translate([0, l/2, 0]) cube([d+3, l, d+3], center=true);
        #rotate([-90,0,0]) translate([0,0,-1])cylinder(d=7.4, h=l+2);
    }
}
