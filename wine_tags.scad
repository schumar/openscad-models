// bottle neck diameter
bd = 30;

// opening angle
oa = 100;

// strength
w = 2;

// tag width
tw = 14;

// tag height
th = 28;

// font size
fs = 16;

// year -- first line
yt = "20x";
// year -- second line
yb = "3";


rotate([0,0,90+oa/2]) rotate_extrude(angle=360-oa, $fa=5) {
    translate([bd/2, 0]) union() {
        circle(w/2, $fs=0.1, $fa=20);
        translate([-w/2, -w/2]) square([w, w/2]);
    }
}

difference() {
    translate([-tw/2, -bd/2 - th, -w/2]) cube([tw, th+bd/2, w]);
    translate([0,0,-w]) cylinder(d=bd, h=2*w);
    #translate([0, -bd/2-2, 0]) linear_extrude(w)
        text(yt, size=6, halign="center", valign="top", font="3270 Condensed");
    #translate([0, -bd/2-th+2, 0]) linear_extrude(w)
        text(yb, size=12, halign="center", valign="bottom");
}
