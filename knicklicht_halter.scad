$fs = 0.1;

// diameter of Knicklicht
d = 5.7;  // [4:0.2:6]

// radius of Stop
x = 0.3;

// diameter of Seil
s = 2.5;  // [1:0.2:5]

// thickness
t = 2;  // [1:0.5:10]

//wall width
w = 1;

// holder length
l = 10;

// holder thickness
h = 2;

// slit
o = s-1;

linear_extrude(t) {
    difference(){
        union(){
            circle(d=d+3);
            translate([-l+3 ,(d)/2]) square([2*l-6, 2*h+o]);
//            translate([-l ,(d)/2+4]) square([2*l, 2]);
            translate([l-3 ,(d)/2+h+o/2]) circle(d=2*h+o);
            translate([-l+3 ,(d)/2+h+o/2]) circle(d=2*h+o);
        }
        circle(d=d);
        translate([l-3 ,(d)/2+h+o/2]) circle(d=o);
        translate([-l-3 ,(d)/2+h]) square([2*l, o]);
        #translate([l/2, d/2+h+o/2]) circle(d=s);
    }
    translate([0,-d/2]) circle(r=x);
}



