// length of screw (w/o head)
L = 20;
// height of screw head
S = 13;
// head rounding radius
HRR = 4;
// head radius
HR = 16;
// screw stretch
SS = 3;
// screw factor
SF = 0.98;
// epsilon
//e = 0.01;

$fa = 5;
$fs = 0.2;

include <nutsnbolts/cyl_head_bolt.scad>

module T(x=0,y=0,z=0) {
    translate([x,y,z]) children();
}
module Tx(x=0) {
    T(x=x) children();
}
module Ty(y=0) {
    T(y=y) children();
}
module Tz(z=0) {
    T(z=z) children();
}

intersection() {
    Tz(e) scale([SF,SF,SS]) screw(name="M12x25", thread="modeled");
    T(-10,-10,-L) cube([20,20,L]);
}

difference() {
    hull() {
        for (a=[0:120:359]) {
            rotate([0,0,a]) translate([HR-HRR, 0, HRR]) sphere(r=HRR);
            rotate([0,0,a]) translate([HR-HRR, 0, S-HRR+HRR*0.3]) sphere(r=HRR);
        }
    }
    T(-50,-50,S) cube([100,100,100]);
}

