# OpenSCAD Models

Just a bunch of OpenSCAD models I've designed.

Do use the OpenSCAD customizer! ("In the View menu, the option called [Hide customizer] must be unselected to display the customizer." -- <https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Customizer>)

---

## 3018 Limit Switches

![](images/3018.png){width=45%}

Limit switches, emergency button holder, ESP32cam-holder for my 3018.

## Dipol Holder

![](images/dipol_holder.png){width=45%}

Holder for a dipole antenna; you additionally need

* some stiff wire (2 pieces of lambda/4 length)
* BNC connector
* somewhere to screw this thing to (and screws)

## Einstein

![](images/Einstein.png){width=45%}

My rendition of the [Einstein monotile](https://en.wikipedia.org/wiki/Einstein_problem)

## Endoscope Clip

![](images/endoclip.png){width=45%}

Allows you to attach a cheap endoscope camera to your printer bed.

## Fermentation Stamper

![](images/Fermentation_grid.png){width=45%}
![](images/Fermentation_screw.png){width=45%}

Adjustable, ensures that the vegetables to ferment (e.g. chili) are
pressed down into the brine.

## Filter Holder

![](images/filter_holder.png){width=45%}

Generic box for filters (camera-filters, telescope-filters,...)

## Flavour Rack

![](images/flavour_rack.png){width=45%}

Bottle rack.

## Fairphone Charger

![](images/fp_charger.png){width=45%}

Add one of those cheap USB-Li-charger modules, and you got yourself
a battery charger for Fairphone 2 (customizable, so you can use it for
other phones too)

## KeyForge Inserts

![](images/KeyForge_inserts.png){width=45%}

Little boxes, can be put into a KeyForge paper box

## Knicklichthalter

![](images/knicklicht_halter.png){width=45%}

Allows you to attach snap-light-sticks to a guy rope (so people don't
fall over it in the night).

## LabelMakerTape Holder

Box for Brother labelmaker-tapes.

## LED Torch

![](images/led_torch.png){width=45%}

Slightly over-engineered LED torch. With USB-charger, 18650 battery,
voltage control.

## Printer Case Corner

![](images/printer_case_corner.png){width=45%}

Print 4 of those, and then 4 again, but mirrored(!), connect with wooden
bars, put aluminium foil over it. Cheap "printer case" :)

## PSU Case

![](images/psu_case.png){width=45%}

Case for my DP5005-based bench top power supply

## SMD Cartridge

![](images/SMD_tape_cartridge.png){width=45%}

An insert for the [SMD organizer by robin7331](https://www.thingiverse.com/thing:3952021),
done in OpenSCAD (based on [rewolff's version](https://github.com/rewolff/SMD_tape_cartridge)),
with a compliant front connector (based on [agapios' model](https://www.thingiverse.com/thing:4511414))

## Spectrometer

![](images/spectrometer.png){width=45%}

Build a simple spectrometer, using a square piece cut out from a CD.

## Wine Tags

![](images/wine_tags.png){width=45%}

Attach to top of wine bottle, so you know which year the wine was made in
(e.g. use "20x3" for bottles made in 2003, 2013, 2023,...)

## Wine Tags Holder

![](images/wine_tags_holder.png){width=45%}

Box for the Wine Tags.

