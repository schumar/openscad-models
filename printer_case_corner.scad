// broad side
b = 20;
// small side
s = 13.5;
// screw big diameter
sb = 6;
// screw small diameter
ss = 3;

// glass strength
g = 1;
// long slit open?
lso = false;
// short slit open?
sso = false;

// wall width
w = 2;

/* [Hidden] */
$fs = 0.5;

// length
l = s+w;

module holder() {
        difference(){
            cube([l+w, w+b+w, w+s+w]);
            translate([w,w,w]) cube([l+1, b, s]);
            translate([w, w+b*0.4, w+s-0.5]) cube([l+1, b*0.2, w+1]);
            translate([w+s/2, w+b-0.1, w+s/2]) rotate([-90,0,0]) #cylinder(d1=ss, d2=sb, h=w+0.2, $fa=5, $fs=0.5);
        }
}

holder();
translate([b-s,0,0]) scale([-1,1,1]) rotate([0,0,-90]) holder();
rotate([0,-90,0]) holder();

translate([-(w+s+w+g+w), -(w+l), 0])
    difference() {
        cube([w, l+w+w+b+w+g+w, w+l]);
        translate([-1, l-s/2, s/2+w]) rotate([0, 90, 0]) #cylinder(d=sb, h=w+2);
    }

if (lso == false) {
    translate([-(w+s+w+g+0.5), -(w+l), 0]) cube([g+1, l+w+w+b+w, w]);
}

translate([-(w+s+w+g+w), w+b+w+g, 0])
    difference() {
        cube([l+w+w+s+w+g+w, w, w+l]);
        translate([w+g+w+s/2, -1, w+s/2]) rotate([-90, 0, 0]) #cylinder(d=sb, h=w+2);
        translate([w+g+w+s+w+w+s/2, -1, w+s/2]) rotate([-90, 0, 0]) #cylinder(d=sb, h=w+2);
    }

if (sso == false) {
    translate([-(w+s+w), (w+b+w-0.5), 0]) cube([l+w+w+s+w, g+1, w]);
}
