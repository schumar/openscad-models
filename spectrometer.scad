// What to print
print = "body"; // [body, top]

// Distance between CD grooves (in nm); 74min CD: 1600, 80min CD: 1480, DVD: 740, HD-DVD: 400, Blu-ray: 320
d = 1480;

// Incident angle (degree)
i = 59.94;

// Highest wavelength (red)
lh = 700;
// Lowest wavelength (blue)
ll = 390;

// Heigth of case
ch = 50;
// Width of case
cw = 50;
// Length of case
cl = 22;
// Color of case
cc = [0.2, 0.2, 0.4, 0.5];
// Wall strength
w = 1.8;
// Trap strength
ts = 1.2;

// slit width
sw = 1;

// CD piece width
pw = 20;
// CD piece height
ph = 20;
// CD piece thickness
pt = 1.2;
// CD laser depth (1.1 for CD, 0.6 for DVD/HD-DVD, 0.1 for Blu-ray)
pm = 1.1;
// CD holder thickness
ht = 1.2;
// CD piece refraction index
pn = 1.55;

// camera opening diameter
cd = 43;

$fs = 0.5;
$fa = 5;

font = "Lato:style=Thin";

// Epsilon
e = 0.01;

module holder() {
    translate([-pw/2, 0, -cw/2+w]) linear_extrude(cw/2-w+ph/2)
        polygon([ [-ht, -ht],
                  [-ht, ht+pt],
                [+2*ht, ht+pt],
                [+2*ht, pt],
                    [0, pt],
                    [0, 0],
                  [+ht, 0],
                  [+ht, -ht]
    ]);
}

// Interference angle within medium
im = asin ( sin(i) / pn );
ix = pm * tan(im);

// Interference angles of low/high wavelengths (in medium)
ilm = asin( sin(im) - ll/pn/d);
ihm = asin( sin(im) - lh/pn/d);

// Interference angles (outside medium)
il = asin ( sin(ilm) * pn );
ih = asin ( sin(ihm) * pn );
imid = (ih+il)/2;

// Optical path
rotate([0, 0, i]) union(){

    // Mark main beam
    for (x=[-sw/2,0,sw/2]) translate([x,0,0]) {
        translate([-ix, -pm, 0]) %rotate([90,0,-i]) cylinder(d=0.2, h=50, $fs=0.1);

    // Main beams within medium
    for (a=[-im, +im])
        %rotate([90,0,a]) cylinder(d=0.2, h=sqrt(ix*ix+pm*pm), $fs=0.1);
    }

    echo(im=im, ihm=ihm, ilm=ilm, ih=ih, il=il, iang=abs(ih-il), imid=imid, iout = i+imid);

    // // Mark interference beam
    // color([0.7, 0.7, 0]) %difference(){
    //     translate([0, 0, -0.1]) cylinder(r=50, h=0.2);
    //     if (il < ih) {
    //         rotate([0, 0, il]) translate([-200, -100, -50]) cube([200,200,100]);
    //         rotate([0, 0, ih]) translate([0, -100, -50]) cube([200,200,100]);
    //     } else {
    //         rotate([0, 0, il]) translate([0, -100, -50]) cube([200,200,100]);
    //         rotate([0, 0, ih]) translate([-200, -100, -50]) cube([200,200,100]);
    //     }
    // }

    // red interference beam
    color([0.9, 0, 0, 0.3])
    for (x=[-sw/2,0,sw/2]) translate([x,0,0]) {
        %rotate([90,0,ihm]) cylinder(d=.2, h=pm/cos(ihm), $fs=0.2);
        %translate([pm * tan(ihm), -pm, 0]) rotate([90,0,ih]) cylinder(d=.2, h=50, $fs=0.1);
    }

    // blue interference beam
    color([0, 0, 0.9, 0.3])
    for (x=[-sw/2,0,sw/2]) translate([x,0,0]) {
        %rotate([90,0,ilm]) cylinder(d=.2, h=pm/cos(ilm), $fs=0.2);
        %translate([pm * tan(ilm), -pm, 0]) rotate([90,0,il]) cylinder(d=.2, h=50, $fs=0.1);
    }

    // green interference beam
    color([0, 0.9, 0, 0.3])
        %translate([pm * tan((ilm+ihm)/2), -pm, 0]) rotate([90,0,imid]) cylinder(d=.2, h=50, $fs=0.1);

}

// Offset for camera
cxoff = - pm * tan((ilm+ihm)/2) * cos( imid );

// Offset for slit
syoff = (pm * tan(i) - ix) * cos(i);

echo (cxoff=cxoff, syoff=syoff);

// Distance between entry ray and outer back wall face
dr = pw/2 * cos(i) + w;

// Distance between outer back wall and front dege of holder
frontedge = dr + (pw/2+ht)*cos(i) + (pm+ht)*sin(i);

// Entry slit width
esw = sw*cos(i);

if ( print == "body") {

    // base plate with engraving
    translate([-dr, -cw/2, -ch/2]) difference(){
        cube([cl, cw, w]);

        #translate([0, 0, w-0.4-e/2]) linear_extrude(0.4+e) rotate([0,0,90]) union(){
            // d and i in upper right
            translate([cw-w -1, -w-1]) text(str("≡ " , d), halign="right", valign="top", font=font, size=3.5);
            translate([cw-w -1, -frontedge+ts+1]) text(str("↛ " , i), halign="right", valign="bottom", font=font, size=3.5);

            // slit width, directly above CD piece
            translate([cw/2, -dr+e]) rotate(i-90) text(str(sw,"mm"), halign="center", valign="bottom", font=font, size=3.3);

            // angle of low/high wavelength (relative to camera)
            txtl = str(ll, "@", round((90-(il+i))*10)/10, "°");
            txth = str(lh, "@", round((90-(ih+i))*10)/10, "°");
            // swap left/right label if necessary
            txtleft = (il > ih) ? txtl : txth;
            txtright = (il > ih) ? txth : txtl;
            translate([cw-w -1, -frontedge-1]) text(txtright, halign="right", valign="top", font=font, size=3.3);
            translate([w +1, -frontedge-1]) text(txtleft, halign="left", valign="top", font=font, size=3.3);
        }
    }

    // CD piece
    rotate([0, 0, i]) translate([0, -pm, 0]) {
        %color([0.3, 0.7, 0.5, 0.3]) translate([-pw/2, 0, -ph/2]) cube([pw, pt, ph]);
        translate([-pw/2, 0, -cw/2+w]) cube([pw, pt, cw/2-w-ph/2]);
        holder();
        mirror([1,0,0]) holder();
    }

    // case
    color(cc) translate([-dr, -cw/2, -ch/2+w]) render() difference() {
        linear_extrude(ch-w) {
            // left wall
            square([cl, w]);
            // back wall
            square([w, cw]);
            // right wall
            translate([0, cw-w]) square([cl, w]);
            // front wall
            translate([cl-w, 0]) square([w, cw]);

            // entry traps
            traplength =( cw-2*w-(pw+2*ht)*sin(i)) / 2;
            translate([0, traplength+w -ts/2]) square([frontedge, ts]);
            translate([0, traplength/2+w -ts/2] ) square([frontedge, ts]);
            translate([frontedge-ts, 0]) square([ts, traplength+w+ts/2]);

            // transmission traps
            translate([frontedge-ts, cw/2 + (pw/2)*sin(i)]) square([ts, traplength+w]);
        }

        // slit for entry beam
        translate([dr-esw/2+syoff, -e, (ch-pw)/2-w+1]) cube([esw, cw/2, pw-2]);

        // camera opening
        translate([cl-w-e/2, cw/2+cxoff, ch/2-w]) rotate([0, 90, 0]) cylinder(d=cd, h=w+e, $fs=0.5, $fa=5);
        // small mark at the top -- ensures better fit (overhang might droop)
        translate([cl-w-e/2, cw/2+cxoff, ch/2-w+cd/2-1.5]) rotate([0, 90, 0]) cylinder(d=4, h=w+e);


        // cut away top
        translate([-e/2, -e/2, ch-w-w/2]) cube([cl+e, cw+e, w/2+e]);
        translate([w/2-e/2, w/2-e/2, ch-w-w]) cube([cl+e-w, cw+e-w, w+e]);
    }

    // transmission trap
    ttw = max(esw*1.2+0.6, 1.8);
    translate([0, cw/2-w, -ch/2]) linear_extrude(ch/2 + pw/2) {
        polygon([ [-ttw/2, 0], [ttw/2, 0], [ttw/2, -ttw*0.8] ]);
    }

} else if (print == "top") {

    color(cc) translate([-dr, -cw/2, ch/2]) union() {
        translate([-e/2, -e/2, -w/2]) cube([cl+e, cw+e, w/2+e]);
        translate([w/2-e/2, w/2-e/2, -w]) cube([cl+e-w, cw+e-w, w+e]);
        for (x=[0,1]) for (y=[0,1]) translate([w/2+x*(cl-2*w), w/2+y*(cw-2*w), -3-w+e]) cube([w,w,3]);
    }
}

// camera
%color([0.1,0.1,0.1,0.2]) translate([cl-dr-6.5, cxoff, 0]) rotate([0, 90, 0]) cylinder(d=cd, h=20);
%color([0.1,0.1,0.1,0.2]) translate([cl-dr+10, cxoff, 0]) rotate([0, 90, 0]) cylinder(d=10, h=5);
