/*
 * TODO:
 * * Typenschild (back)
 * * Mutternhalter fuer PowerSocket
 * * Schaltbild auf top
 * * Power LEDs
 * * Hex cooling holes
 * * an Unterkante 1mm dazu, sonst brechen unten die nubs
 * * Insgesamt etwas hoeher machen
 * * right-plate Mutternhalter unten/hinten stoeszt an PowerSocket-Mutter
 * * Trafo repositioniern
 */

/*
 From top:
  ⇤       bw      ⇥
 ┏━━━━━━━━━━━━━━━━━┓
 ┃ ╭─╮       ┌──┐  ┃ ⤒
 ┃ ╰─╯       │  │  ┃ bd
 ┃ ┌─────┐   └──┘  ┃ ⤓
 ⭙━┷━━━━━┷━━━━━━━━━┛

 */

/* [Render] */
render_front = true;
render_left = true;
render_right = true;
render_back = true;
render_top = true;

/* [Generic settings] */
// wall strength
w = 2;
// epsilon
e = 0.01;
// text size
txts = 4;
// text font
txtf = "Noto Mono";

/* [Base] */
// base width
bw = 168.3;
// base depth
bd = 144.2;
// base thickness
bt = 1.4;

// feet height (incl base board thickness)
fh = 10;

/* [Case] */
// case height
ch = 80;

/* [Front] */
// DPS5005 cutout width
dpsw = 71.5;
// DPS5005 cutout snap width
dpss = 76;
// DPS5005 cutout height
dpsh = 39.2;
// DPS5005 depth (just for planning)
dpsd = 36;
// DPS5005 distance to left edge
dpsx = 10;
// DPS5005 distance to top edge
dpsy = 10.5;
// power switch width
psww = 12.6;
// power switch height
pswh = 18.4;
// power switch snap height
psws = 19.7;
// power switch depth
pswd = 23;
// power switch distance to right edge
pswx = 5;
// power switch distance to top edge
pswy = dpsy + dpsh/2 - pswh/2;
// banana socket diameter
band = 6;
// banana base distance
banb = 3/4 * 25.4;  // 3/4 inch
// banana outer length
bano = 15;
// banana outer diameter
bant = 9;
// banana inner length
bani = 12;
// grounding switch width
gndw = 11.6;
// grounding switch height
gndh = 12.5;
// grounding switch inner length
gndi = 15;
// grounding switch outer length
gndo = 10;
// grounding switch hole diameter
gndd = 6.0;


/* [Transformer] */
// Transformer width
traw = 58.3;
// Transformer depth
trad = 63;
// Transformer height
trah = 70;
// Transformer distance to right edge
trax = 16;
// Transformer distance to back edge
tray = 28;

/* [Power Socket] */
// power socket width
posw = 27;
// power socket height
posh = 19.2;
// power socket rounding radius
posr = 3;
// power socket screw distance
posc = 6.5;

/* [Power Filter] */
// filter width
filw = 80;
// filter height
filh = 50;
// filter screw distance
fils = 60;
// filter backplate distance
filb = 4;
// filter screw diameter
fild = 2.6;

/* [Screws] */
// screw diameter
sd = 3;
// nut outer diameter
nd = 5.4;
// nut height
nh = 4;
// screw offset to wall
so = 5;

module base() {
    translate([0, 0, -bt]) cube([bw, bd, bt]);
}
%base();

module dps5005() {
    translate([dpsx,   -w,   ch-dpsh-dpsy  ]) {
        translate([ 0, -e,  0]) cube([dpsw,   dpsd, dpsh]);
        translate([-4, -2, -2]) cube([dpsw+8, 2, dpsh+4]);
        translate([-(dpss-dpsw)/2, -e, w]) cube([dpss, 10, dpsh-2*w]);
    }
}
%dps5005();

module power_switch() {
    translate([bw-psww-pswx, -w, ch-pswh-pswy]) {
        translate([ 0, -e,  0]) cube([psww, pswd, pswh]);
        translate([-1, -2, -2]) cube([psww+2, 2, pswh+4]);
        translate([w,  -e, -(psws-pswh)/2]) cube([psww-w*2, 10, psws]);
    }
}
%power_switch();

module banana() {
    rotate([90,0,0]) {
        cylinder(d=bant, h=bano);
        translate([0, 0, -bani+e]) cylinder(d=band, h=bani, $fn=14);
    }
}

module gnd_switch() {
    translate([0,-e,0]) cylinder(d=gndd, h=gndo, $fn=12);
    translate([-gndw/2, -gndh/2, -gndi]) cube([gndw, gndh, gndi]);
}
// !gnd_switch();

module transformer() {
    translate([bw-traw-trax, bd-trad-tray, 0]) cube([traw, trad, trah]);
}
%transformer();

module power_socket() {
    $fn=24;
    linear_extrude(15)
        offset(r=posr) offset(delta=-posr) square([posw, posh]);
    linear_extrude(6)
        for (x=[-posc, posw+posc]) translate([x, posh/2]) circle(d=3, $fn=12);
}
// !power_socket();

module filter() {
    translate([bw-filw, bd-filb, posh]) rotate([90,0,0])
    linear_extrude(1.6) difference() {
        square([filw, filh]);
        translate([(filw-fils)/2, filh/2]) circle(d=fild, $fn=12);
        translate([filw-(filw-fils)/2, filh/2]) circle(d=fild, $fn=12);
    }
}
%filter();

module nut_holder() {
    difference() {
        linear_extrude(nh+2*w) {
            translate([0, -e]) square([2*so, so+e]);
            translate([so, so]) circle(r=so);
        }
        #translate([so, so, w]) {
            translate([0,0,-w-e/2]) cylinder(d=sd/.866, h=nh+2*w+e, $fn=6);
            linear_extrude(nh) {
                rotate(30) circle(d=nd*1.155, $fn=6);
                translate([-nd/2,0]) square([nd, nd]);
            }
        }
    }
}
//!nut_holder();

module nub() {
    linear_extrude(w) {
        difference() {
            union() {
                translate([0, -e]) square([2*so, so+e]);
                translate([so, so]) circle(r=so);
            }
            translate([so, so]) circle(d=sd/.866, $fn=6);
        }
    }
}
//!nub();

module rubber_foot() {
    translate([0,0,-fh+e]) difference(){
        cylinder(d1=16, d2=20, h=fh);
        translate([0,0,-e]) #cylinder(d=6,h=fh-4);
    }
}
for (x=[12, bw-12]) for (y=[23, bd-23]) translate([x,y,0]) %rubber_foot();

module side() {
    // plate
    translate([-w, 0, -bt-w]) cube([w, bd, ch+bt+w]);
    // nut holder
    translate([0, 0, ch-w])                  rotate([0,180,-90]) nut_holder();  // front top
    translate([0, 0, 0])     mirror([0,1,0]) rotate([0,0,-90]) nut_holder();    // front bottom
    translate([0, bd, ch-w]) mirror([0,0,1]) rotate([0,0,-90]) nut_holder();    // back top
    translate([0, bd, 0])                    rotate([0,0,-90]) nut_holder();    // back bottom
}

if (render_left) {
    side();
} else {
    // %side();
}

translate([bw,0,0]) mirror([1,0,0])
    if (render_right) {
        side();
    } else {
        // %side();
    }

module front_plate() {
    translate([-w, -w, -bt-w]) cube([w+bw+w, w, w+bt+ch]);
    for (x=[0, bw-2*so]) for (z=[-w-bt, ch-w])
        translate([x, 0, z]) nub();
}

// Height of banana sockets
basez = (ch-dpsy-dpsh-2)/2;
// x-center of unregulated outputs
rawx = dpsx + dpsw + 1.5*banb;

module label(txt, l, h, v) {
    rotate([90,0,0]) linear_extrude(l) text(txt, size=txts, font=txtf, halign=h, valign=v, $fn=24);
}

module front() {
    difference() {
        union() {
            front_plate();
            // Power Switch fix
            translate([bw-psww-pswx-w, -e, ch-pswh-pswy-w+e/2])
                cube([2*w, 5, pswh+2*w-e]);
            translate([bw-pswx-w, -e, ch-pswh-pswy-w+e/2])
                cube([2*w, 5, pswh+2*w-e]);
            // Grounding Switch fix
            translate([(rawx+bw)/2 - gndw/2 -w, -e, basez-gndh/2-w])
                cube([gndw+2*w, 5, gndh+2*w]);
            // DPS fix
            translate([dpsx-w, -e, ch-dpsy-dpsh-w]) cube([dpsw+2*w, 5, dpsh+2*w]);
            // strengtheners
            translate([dpsx+(dpsw-w)/2, 0, 0]) cube([w, w, ch]);
            translate([dpsx+dpss, 0, 0]) cube([w, w, ch]);
            translate([0, 0, basez-8-w]) cube([bw, w, w]);
            translate([0, 0, basez+8]) cube([bw, w, w]);
            difference(){
                translate([0, 0, ch-w]) cube([bw, w, w]);
                translate([dpsx+dpsw+8,-e/2,ch-w-e/2]) cube([2,w+e,w+e]);
            }
        }

        dps5005();
        power_switch();

        // Output
        translate([0, -w-e, basez]) {
            // Regulated output (2 times)
            for (n=[0:3])
                translate([dpsx+ (dpsw-3*banb)/2 +n*banb, 0, 0]) #banana();
            // Unregulated (34V DC, 16V AC, 24V AC)
            for (i=[0:2]) for (j=[-1,1])
                translate([rawx + j*banb/2, 0, i*22]) #banana();
            // Earth
            translate([bw-banb/2, 0, 0]) #banana();

        }

        // Grounding Switch
        translate([ (rawx + bw) / 2, 0, basez])
            rotate([90,0,0]) #gnd_switch();

        // Cut away nut holders of side plates
        translate([0, 0, ch-w])                  rotate([0,180,-90]) nut_holder();  // front top
        translate([0, 0, 0])     mirror([0,1,0]) rotate([0,0,-90]) nut_holder();    // front bottom
        translate([bw, 0, ch-w])  mirror([1,0,0]) rotate([0,180,-90]) nut_holder();  // front top
        translate([bw, 0, 0])     rotate([0,0, 90]) nut_holder();    // front bottom

        // Labels
        // regulated output
        translate([dpsx+(dpsw-3*banb)/2, -w+0.2, basez+8]) {
            for (n=[0,2])
                translate([n*banb,0,0]) label("+", 1, "center", "baseline");
            for (n=[1,3])
                translate([n*banb,0,0]) label("0", 1, "center", "baseline");
        }
        // un-regulated output
        translate([rawx, -w+0.2, basez+8]) {
            translate([-banb/2,0,0]) label("32", 1, "center", "baseline");
            translate([+banb/2,0,0]) label("0", 1, "center", "baseline");
            translate([0,0,22])    label("~ 16 ~", 1, "center", "baseline");
            translate([0,0,44])    label("~ 24 ~", 1, "center", "baseline");
        }
        // Earth
        translate([bw-banb/2, -w+0.2, basez+8]) label("E", 1, "center", "baseline");
        // Grounding switch
        translate([(rawx+bw)/2, -w+0.2, basez+6]) label("float", 1, "center", "baseline");
        translate([(rawx+bw)/2, -w+0.2, basez-6]) label("1MOhm", 1, "center", "top");

        // Lines
        // Ground
        translate([dpsx+(dpsw-3*banb)/2+banb, -w-0.8, basez-10]) {
            cube([rawx+banb/2 - (dpsx+(dpsw-3*banb)/2) -banb, 1, 1]);
            translate([-0.5,0,0]) cube([1,1,5]);
            translate([2*banb-0.5,0,0]) cube([1,1,5]);
        }
        translate([rawx+banb/2-0.5, -w-0.8, basez-10]) cube([1,1,5]);
        // regulated
        translate([dpsx+(dpsw-3*banb)/2-0.5, -w-0.8, basez+5]) {
                                     cube([1,1,2]);
            translate([0,     0, 8]) cube([1,1,3]);
            translate([2*banb,0, 0]) cube([1,1,2]);
            translate([2*banb,0, 8]) cube([1,1,3]);
        }
        // grounding switch
        translate([rawx+banb/2+5, -w-0.8, basez-0.5])
            cube([(rawx + bw) / 2 - (rawx+banb/2) -9, 1, 1]);
        translate([bw-banb/2-10, -w-0.8, basez-10]) cube([10, 1, 1]);
        translate([bw-banb/2-0.5, -w-0.8, basez-10]) cube([1, 1, 5]);
    }
}

if (render_front) {
    front();
} else {
    %front();
}

module back() {
    translate([bw, bd, 0]) rotate([0,0,180]) {
        difference() {
            front_plate();
            translate([posc+5, -w-e, posh]) rotate([-90,0,0]) #power_socket();
        }
    }
    translate([bw, bd, posh]) {
        // filter screws
        for (x=[(filw-fils)/2, filw-(filw-fils)/2]) {
            translate([-x, 0, filh/2]) rotate([90,0,0]) difference(){
                cylinder(d=fild+4, h=filb+e, $fn=12);
                translate([0,e,0]) #cylinder(d=fild, h=filb+1.6, $fn=6);
            }
        }
        // filter stabilizers
        for (x=[-w, -filw])
        translate([x, -filb, 0]) cube([w, filb+e, filh]);
    }
    // strengtheners
    translate([bw-posw-2*posc-10-w, bd-w+e, 0]) cube([w, w, ch]);
    translate([0,                   bd-w+e, posh]) cube([bw, w, w]);
}

if (render_back) {
    back();
} else {
    %back();
}

module top() {
    translate([0,0,ch]) {
        difference() {
            translate([-w,-w,0]) cube([w+bw+w, w+bd+w, w]);
            // screw holes
            for (x=[so, bw-so]) for (y=[so, bd-so])
                #translate([x, y, -e/2]) rotate([0,0,15]) cylinder(d=sd*1.035, h=w+e, $fn=12);
            // cooling slots above DPS
            for (x=[0:2.4:dpsw-1.2])
                translate([dpsx+x, dpsd-18, -e/2]) cube([1.2,20,w+e]);
            // cooling slots above transformer
            for (x=[1.2:2.4:traw])
                translate([bw-trax-x, bd-tray-trad/2-20, -e/2]) cube([1.2,40,w+e]);
        }
        // strengtheners
        translate([0,dpsd+10,-4]) cube([bw,2,4]);
        translate([dpsx+dpsw+8,0,-4]) cube([2,bd,4]);
    }

}

if (render_top) top();
