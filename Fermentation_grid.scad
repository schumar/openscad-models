// diameter
D = 69;
// height of grid
H = 3;
// height of screw holder
HS = 8;
// width of outer rim
WO = 4;
// grid hole wall width
W = 0.45*3;
// grid hole diameter
DG = 6;
// screw stretch
SS = 3;
// epsilon
//e = 0.01;

$fa = 5;
$fs = 0.1;

include <nutsnbolts/cyl_head_bolt.scad>

module T(x=0,y=0,z=0) {
    translate([x,y,z]) children();
}
module Tx(x=0) {
    T(x=x) children();
}
module Ty(y=0) {
    T(y=y) children();
}
module Tz(z=0) {
    T(z=z) children();
}

module grid(cw, ch) {
    // math stuff for hexagons -- https://en.wikipedia.org/wiki/Hexagon
    ri = (DG-W)/2;                // radius of inscribed circle of hole
    R  = ri/sqrt(3)*2;            // circumradius of hole
    h  = 1.5 * DG/2/sqrt(3)*2;    // height of each row (with wall)
    nx = ceil(cw/2 / DG);         // how many holes per row (-nx:nx)
    ny = ceil(ch/2 / h);          // how many holes per column (-ny:ny)

    for (x=[-nx:nx]) for (y=[-ny:ny]) {
        translate([x*DG + (y % 2 ? DG/2 : 0),  y*h])
            rotate(30) circle($fn=6, r=R);
    }
}
difference() {
    union() {
        // grid
        difference() {
            cylinder(d=D, h=H);
            Tz(-e) linear_extrude(H+2*e) grid(D,D);
        }
        // screw holder
        cylinder(d=20,h=HS);
    }
    scale([1,1,SS]) Tz(HS/SS+e) hole_threaded(name="M12", l=HS/SS+2*e, thread="modeled");
}

difference() {
    cylinder(d=D, h=H);
    Tz(-e) cylinder(d=D-2*WO, h=H+2*e);
}


