/*
 * [TODO]
 * bessere Luftfuehrung ueber Cooler
 * Blendenring
 * Loch fuer Poti
 * USB-Aussparung kleiner
 * Loch fuer Charge-LEDs
 */

// total length
total_l = 89;
// wall width
w = 2.1; // [1.2:0.1:3.6]

// Solid case?
case_solid = true;
// Upper part?
case_upper = true;
// Lower part?
case_lower = true;

/* [LED] */
// LED diameter
led_dia = 8;
// LED connector width
led_cw = 3.5;
// LED diameter with connectors
led_cd = 12.5;
// LED height (body)
led_h = 2.5;
// LED lens diameter
led_ld = 5;

/* [Battery] */
// battery length
bat_l = 70;
// battery diameter
bat_d = 18.5;
// battery wire hole diameter
bat_wd = 3;

/* [Charger] */
// charger width
crg_w = 16.7;
// charger length
crg_l = 27.8;
// charger board thickness
crg_brd = 1;
// charger USB height (above board)
crg_usb_h = 2.6;
// charger USB width
crg_usb_w = 7;
// charger USB length
crg_usb_l = 6;
// charger free side start
crg_free_s = 14;
// charger free side length
crg_free_l = 9;

/* [Regulator] */
// voltage regulator width
reg_w = 21.082;
// voltage regulator length
reg_l = 43.18;
// regulator board thickness
reg_brd = 1.6;
// regulator cap diameter
reg_cap_d = 7;
// regulator cap height
reg_cap_h = 10;
// regulator inductor width
reg_ind_w = 12;
// regulator inductor height
reg_ind_h = 7;
// regulator potentiometer width
reg_pot_w = 5;
// regulator potentiometer height
reg_pot_h = 12;
// regulator hole diameter
reg_hol_d = 3;
// regulator holes x distance (to short edge)
reg_hol_x = 6.604;
// regulator holes y distance (to long edge)
reg_hol_y = 2.54;

/* [Switch] */
// switch width
swi_w = 12.8;
// switch length
swi_l = 13;
// switch height (body with connectors)
swi_h = 14;
// switch hole diameter
swi_d = 5.8;
// switch key width
swi_kw = 1;
// switch key depth
swi_kd = 0.5;

/* [Clips] */
// clip height
clp_h = 10;
// clip strength
clp_s = 3;
// clip block height
clp_bh = 6;
// clip block strength
clp_bs = 1.2;
// clip connector height
clp_ch = 7;
// clip length (x direction)
clp_l = 10;

/* [LED Cooler] */
// cooler width
col_w = 6.4;
// cooler length
col_l = 18.8;
// cooler height
col_h = 4.6;


// Hide following variables from Customizer
module noop(){}

// voltage regulator max height
reg_h = reg_brd + max(reg_cap_h, reg_ind_h, reg_pot_h);

// decide height of charger with switch above
modules_h = max(crg_brd + 2 + swi_h, reg_h);

assert(w + crg_l + 2 + reg_l + w < total_l, "Can't fit charger and regulator into case");
assert(w + bat_l + 2 + led_h     < total_l, "Can't fit battery and LED into case");
assert(clp_ch > clp_s,                      "Clip would need support -- increase clp_ch");
assert((reg_w-swi_w)/2 > clp_s+clp_bs,      "Clip too thick -- decrease clp_bs or clp_s");

// Epsilon
e = 0.01;

module bat() {
    conn_h = (bat_l-65)/2;
    cylinder(d=bat_d-4, h=conn_h);
    translate([0,0,conn_h]) cylinder(d=bat_d, h=65, $fs=0.5, $fa=5);
    translate([0,0,bat_l-conn_h]) cylinder(d=bat_d-4, h=conn_h);
}

%translate([0, 0, -bat_d/2-bat_wd]) rotate([0,90,0]) bat();

module crg() {
    translate([0, -crg_w/2, 0]) cube([crg_l, crg_w, crg_brd]);
    translate([0, -crg_usb_w/2, crg_brd]) cube([crg_usb_l, crg_usb_w, crg_usb_h]);
    translate([4, -crg_w/2+1.7, crg_brd-e/2]) cube([crg_free_s-4, crg_w-1.7, 2]);
}
%crg();

module reg() {
    difference() {
        translate([0, -reg_w/2, 0]) cube([reg_l, reg_w, reg_brd]);
        translate([reg_hol_x, reg_w/2-reg_hol_y, -e/2]) cylinder(d=reg_hol_d, h=reg_brd+e, $fa=5, $fs=0.5);
        translate([reg_l-reg_hol_x, -reg_w/2+reg_hol_y, -e/2]) cylinder(d=reg_hol_d, h=reg_brd+e, $fa=5, $fs=0.5);
    }
    // input capacitor
    translate([1+reg_cap_d/2, 0, reg_brd]) cylinder(d=reg_cap_d, h=reg_cap_h);
    // output capacitor
    translate([reg_l-1-reg_cap_d/2, 0, reg_brd]) cylinder(d=reg_cap_d, h=reg_cap_h);
    // inductor
    translate([2+reg_cap_d, -reg_w/2+1, reg_brd]) cube([reg_ind_w, reg_ind_w, reg_ind_h]);
    // potentiometer
    translate([2+reg_cap_d, reg_w/2-1-reg_pot_w, reg_brd]) cube([reg_ind_w, reg_pot_w, reg_pot_h]);
    // little SMD cap (cutting into peg)
    translate([reg_l-8, reg_cap_d/2, reg_brd]) cube([2, 3.5, 1]);
}
%translate([total_l-2*w-reg_l, 0, 0]) reg();

module swi() {
    translate([0, -swi_w/2, -swi_h]) cube([swi_l, swi_w, swi_h]);
    difference(){
        translate([+swi_l/2, 0, 0]) cylinder(d=swi_d, h=9, $fs=1);
        translate([swi_l/2-swi_d/2-e, -swi_kw/2, -e/2]) cube([swi_kd, swi_kw, 10]);
    }
}
%translate([total_l-2*w-reg_l-swi_l, 0, modules_h]) swi();

module led() {
    cylinder(d=led_dia, h=led_h, $fs=0.5, $fa=5);
    translate([0,0,led_h]) sphere(d=led_ld, $fs=1);
    intersection() {
        cylinder(d=led_cd, h=led_h, $fs=0.5, $fa=5);
        translate([-(led_cd+e)/2, -led_cw/2, -e/2]) cube([led_cd+e, led_cw, led_h/2+e]);
    }
}
%translate([total_l-2*w, 0, -bat_d/2-bat_wd]) rotate([90,0,90]) led();

// cooler
module col() {
    translate([-col_h, -col_w/2, 0]) cube([col_h, col_w, col_l]);
}
%translate([total_l-2*w, 0, -bat_d-bat_wd]) col();

// center of clip (in x direction)
clp_cx = (total_l-2*w-reg_l+crg_l)/2;

module clip_top() {
    translate([clp_cx-clp_l/2, -reg_w/2, 0]) rotate([90,0,90]) linear_extrude(clp_l) polygon([
            [-e,   0],
            [-e,   clp_bh],
            [clp_bs, clp_bh-clp_bs],
            [clp_bs, 0],
    ]);

}
module clip_bottom() {
    translate([clp_cx-clp_l/2, -reg_w/2, 0]) rotate([90,0,90]) linear_extrude(clp_l) polygon([
            [-e,     0],
            [clp_bs, 0],
            [clp_bs, clp_bh-clp_bs],
            [0,      clp_bh],
            [clp_s-1, clp_h],
            [clp_s,   clp_h],
            [clp_s,   0],
            [0, -clp_ch],
    ]);
}

module form() {
    bat_r = bat_d/2;
    polygon([
            [-reg_w/2, 0],
            [-reg_w/2, reg_brd],
            [-reg_w/2+1, reg_brd+reg_pot_h],
            [-swi_w/2, modules_h],

            [ swi_w/2, modules_h],
            [ reg_w/2-1, reg_brd+reg_ind_h],
            [ reg_w/2, reg_brd],
            [ reg_w/2, 0],

            [ bat_r, -bat_r-bat_wd],
            [ bat_r/sqrt(2), -bat_wd-bat_r*(1+1/sqrt(2))],
            [ bat_r * (sqrt(2)-1), -bat_wd-bat_d],

            [-bat_r * (sqrt(2)-1), -bat_wd-bat_d],
            [-bat_r/sqrt(2), -bat_wd-bat_r*(1+1/sqrt(2))],
            [-bat_r, -bat_r-bat_wd],
    ]);
    translate([0, -bat_d/2-bat_wd]) circle(d=bat_d, $fs=0.5, $fa=5);
}

module case() {
    // form, length
    translate([total_l-w, 0, 0]) rotate([90,0,-90]) linear_extrude(total_l) difference() {
        offset(delta=w, chamfer=false) form();
        form();
    }
    // form, caps
    rotate([90,0,-90]) linear_extrude(w)
        offset(delta=w, chamfer=false) form();
    translate([total_l-w, 0, 0]) rotate([90,0,-90]) linear_extrude(w)
        offset(delta=w, chamfer=false) form();
    // peg above charger -- left
    translate([crg_l-5, 0, e]) rotate([90,0,-90]) linear_extrude(crg_l-5) {
        polygon([[-reg_w/2,  0],
                 [-reg_w/2, 10],
                 [-crg_w/2+1.5, crg_brd],
                 [-crg_w/2+1.5, 0],
                 [-reg_w/2, -10],
        ]);
    }
    // peg above charger -- right
    translate([crg_l-5, 0, e]) rotate([90,0,-90]) linear_extrude(crg_l-5) {
        polygon([[ reg_w/2,  0],
                 [ reg_w/2, 10],
                 [ crg_w/2-1.5, crg_brd],
                 [ crg_w/2-1.5, 0],
                 [ reg_w/2, -10]
        ]);
    }
    // pegs at back of charger
    translate([crg_l+1.5, 0, e]) rotate([90,0,-90]) linear_extrude(3) {
        polygon([[-reg_w/2,  0],
                 [-reg_w/2, 10],
                 [-crg_w/2+1.8, 0]
        ]);
        polygon([[ reg_w/2,  0],
                 [ reg_w/2, 10],
                 [ crg_w/2-1.8, 0]
        ]);
    }
    // pegs at back of regulator
    translate([total_l-2*w-reg_l+reg_cap_d+2, 0, e]) rotate([90,0,-90]) linear_extrude(reg_cap_d+2-3.5) {
        polygon([[-reg_w/2,  0],
                 [-reg_w/2, 10],
                 [-reg_w/2+5, reg_brd],
                 [-reg_w/2+5, 0],
                 [-bat_d/2,  0],
                 [-reg_w/2, -10],
        ]);
        polygon([
                 [ reg_w/2,  0],
                 [ reg_w/2, 10],
                 [ reg_w/2-5, reg_brd],
                 [ reg_w/2-5,  0],
                 [ bat_d/2,  0],
                 [ reg_w/2, -10],

        ]);
    }
    // pegs at front of regulator
    translate([total_l-2*w-3.5, 0, e]) rotate([90,0,-90]) linear_extrude(reg_cap_d+2-3.5) {
        polygon([[-reg_w/2,  0],
                 [-reg_w/2, 10],
                 [-reg_w/2+5, reg_brd],
                 [-reg_w/2+5, 0],
                 [-bat_d/2,  0],
                 [-reg_w/2, -10],
        ]);
        polygon([[ reg_w/2,  0],
                 [ reg_w/2, 10],
                 [ reg_w/2-5, reg_brd],
                 [ reg_w/2-5, 0],
                 [ bat_d/2,  0],
                 [ reg_w/2, -10],
        ]);
    }
    // cooler base
    translate([total_l-2*w-col_h-w, -col_w/2-w, -bat_d-bat_wd-e]) cube([col_h+w+e, col_w+2*w, w]);
    // cooler back
    translate([total_l-2*w-col_h-w+e,-col_w/2, -bat_d-bat_wd-e]) rotate([90,0,180]) linear_extrude(col_w) polygon([ [0,0], [0,bat_d/2+led_dia/2], [2*w, 0] ]);
    translate([total_l-2*w-col_h-w,  -col_w/2, -bat_d-bat_wd-e]) cube([w+e, col_w, bat_d/2+led_dia/2]);
    // cooler sides
    translate([total_l-2*w+e, col_w/2+w, -bat_d-bat_wd+col_l-e]) rotate([90,180,0]) linear_extrude(col_w+2*w) polygon([ [0,0], [2,0], [0,3] ]);
}

module cutter() {
    translate([-w-e/2, -(reg_w+e)/2-w, 1]) cube([total_l+e, reg_w+2*w+e, modules_h+w]);
    translate([-w/2-e/2, -(reg_w+e+w)/2, 0]) cube([total_l-w+e, reg_w+w+e, modules_h+w]);
}

module case_cut() {
    difference() {
        case();
        // hole for switch
        translate([total_l-2*w-reg_l-swi_l, 0, modules_h]) swi();
        // USB hole
        translate([-w-e/2, -5, crg_brd+crg_usb_h/2-3]) cube([w+e, 10, crg_usb_h+3]);
        // charger
        translate([0,0, -e/2]) crg();
        // regulator
        translate([total_l-2*w-reg_l, 0, -e/2]) reg();
        // LED
        translate([total_l-2*w-e, 0, -bat_d/2-bat_wd]) rotate([90,0,90]) led();
        // cooler
        translate([total_l-2*w-e, 0, -bat_d-bat_wd]) col();
        // cooling holes bottom
        for (y=[-col_w/2, +col_w/2]) for (x=[bat_l+1:5.5:total_l-2*w-1])
            translate([x,y,-bat_d-bat_wd-w-e/2]) cylinder(d=2, h=w+bat_d/2+led_dia/2+e, $fn=6);
        // cooling holes front
        for (y=[-1, +1])
            translate([total_l-2*w-e/2, y*(col_w/2+w+2), -3]) rotate([0,90,0]) rotate([0,0,30]) cylinder(d=4, h=w+e, $fn=6);
        // holes to open clips
        translate([clp_cx, reg_w/2+5, clp_bh+1]) rotate([90,0,0]) cylinder(d=2, h=reg_w+10, $fn=6);

    }
}

if (case_solid) {
    if (case_upper)
        color([0.7, 0.7, 0.3]) union() {
            intersection() { case_cut(); cutter(); }
            clip_top(); mirror([0,1,0]) clip_top();
        }
    if (case_lower)
        color([0.8, 0.6, 0.3]) union() {
            difference()   { case_cut(); cutter(); }
            clip_bottom(); mirror([0,1,0]) clip_bottom();
        }
} else {
    if (case_upper)
        %color([0.7, 0.7, 0.3, 0.3]) render() {
            intersection() { case_cut(); cutter(); }
            clip_top(); mirror([0,1,0]) clip_top();
        }
    if (case_lower)
        %color([0.8, 0.6, 0.3, 0.3]) render() {
            difference()  { case_cut(); cutter(); }
            clip_bottom(); mirror([0,1,0]) clip_bottom();
        }
}

