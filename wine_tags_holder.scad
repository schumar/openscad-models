// wall strength
w = 2;

// tag width
tw = 14 + 0.5;

// tag height
th = 18;

// tag strength
ts = 2 + 0.2;

// bottle neck diameter
bd = 30 + 2*ts;

// font size
fs = 16;

// epsilon
e = 0.01;

difference(){
    cube([w+tw+w+bd+w, w + 5*(6*ts+w), w+th]);
    // Aussparungen
    for (n=[0:4]) for (x=[0:1])
        translate([w + x*(w+bd), w + n*(6*ts+w), w]) #cube([tw, 6*ts, th+e]);
    // Mitte
    translate([w + tw + w, w, w]) cube([bd-tw-w, 5*(6*ts+w) - w, th+e]);
    // Text links
    for (n=[0:4])
        translate([1, w + n*(6*ts+w) + 3*ts, w]) rotate([90,0,-90]) #linear_extrude(w)
            text(str(n), size=fs, halign="center", valign="baseline"); // , font="3270 Condensed");
    // Text rechts
    for (n=[0:4])
        translate([w+tw+w+bd+w-1, w + n*(6*ts+w) + 3*ts, w]) rotate([90,0,90]) #linear_extrude(w)
            text(str(5+n), size=fs, halign="center", valign="baseline"); //, font="3270 Condensed");
    // Text vorne
    for (x=[0:1])
        translate([w+ tw/2 + x*(w+bd), 1, w]) rotate([90,0,0]) #linear_extrude(w)
            text(str(5*x), size=fs, halign="center", valign="baseline"); //, font="3270 Condensed");
    // Text hinten
    for (x=[0:1])
        translate([w+ tw/2 + x*(w+bd), w + 5*(6*ts+w) - 1, w]) rotate([90,0,180]) #linear_extrude(w)
            text(str(4+5*x), size=fs, halign="center", valign="baseline"); //, font="3270 Condensed");
}
