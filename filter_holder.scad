// render
render = "bottom"; // [top, bottom]

// text size
txts = 8;
// text font
txtf = "Noto Mono";

// labels
label = ["CPL", "UV", "ND2", "ND4", "ND8"];

// filter length
fll = [7.6, 5.7, 6.7, 6.7, 6.7];
assert(len(label) == len(fll));
// filter diameter
fld = 43;
// filter protection thickness
flp = 1.5;
// number of filters
nfl = len(label);
// filter border
flb = 2;

// wall strength
w = 1.7;

// case width
cw = fld - 2*flb;
// case length
cl = w + 3*w + flp + nfl*flp + sumv(fll, len(fll)-1) + 3*w + w;

echo (cl=cl);

// epsilon
e = 1/128;

$fs = 0.5;
$fa = 5;

// find the sum of the values in a vector (array)
// from the start (or s'th element) to the i'th element
function sumv(v,i,s=0) = (i<s ? 0 : i==s ? v[i] : v[i] + sumv(v,i-1,s));

fllsum = sumv(fll, len(fll)-1);

// module filter_hole(x=0) {
//     translate([x - fll/2, 0, flr+w]) rotate([0,90,0]) cylinder(d=fld, h = fll);
// }
// rotate([0,90,0]) cylinder(d = fld-2, h = 2*flp + fll);

module nub_front() {
    translate([-cw/2+w+0.5, 0, fld/2-4*w]) rotate([90, 0, 90]) linear_extrude(cw-2*(w+0.5))
        polygon([[1, 0], [-0.8, w], [-0.8, 2*w], [e, 2*w + (w-1.1)],
                [e, 4*w],
                [e-w, 4*w+fld/2], [2*w, 4*w+fld/2], [w, 0]
        ]);
}

module bottom() {
    difference() {
        translate([-cw/2, -w, -w]) cube([cw, cl, w + fld/2]);
        translate([-cw/2+w, 0, 0]) cube([cw-2*w, 3*w, fld/2+e]);
        translate([-cw/2+w, cl-5*w, 0]) cube([cw-2*w, 3*w, fld/2+e]);
        for (i=[0:nfl-1]) {
            translate([0, 3*w + flp + i*flp + sumv(fll, i-1), fld/2]) rotate([-90,0,0]) cylinder(d=fld, h=fll[i]);
        }
        nub_front();
        translate([0,cl-2*w,0]) rotate([0,0,180]) nub_front();

        translate([0, 3*w+flp+0.5, -0.2]) linear_extrude(5) for (i=[0:nfl-1]) {
            #translate([0, i*flp + sumv(fll, i-1)]) text(label[i], size=fll[i]-1, font=txtf, halign="center", valign="bottom", $fn=24);
        }
    }
}
if (render == "bottom") {
    bottom();
} else {
    %bottom();
}

d = (cl - 2*w - nfl*txts) / (nfl - 1) + txts;

module top() {
    difference() {
        translate([-cw/2, -w, fld/2]) cube([cw, cl, w+fld/2]);
        translate([-cw/2+w, -w-e, fld/2-e]) cube([cw-2*w, 4*w+e, fld/2]);
        translate([-cw/2+w, cl-5*w, fld/2-e]) cube([cw-2*w, 4*w+e, fld/2]);
        for (i=[0:nfl-1]) {
            translate([0, 3*w + flp + i*flp + sumv(fll, i-1), fld/2]) rotate([-90,0,0]) cylinder(d=fld, h=fll[i]);
        }

        translate([0, 0, fld+w-0.2]) linear_extrude(1) for (i=[0:nfl-1]) {
            translate([0, i*d]) text(label[i], size=txts, font=txtf, halign="center", valign="bottom", $fn=24);
        }
    }
    //translate([-cw/2+w+1, 0, w]) cube([cw-2*(w+1), w, fld-w]);
    nub_front();
    translate([0,cl-2*w,0]) rotate([0,0,180]) nub_front();
}
if (render == "top") {
    top();
} else {
    %top();
}

