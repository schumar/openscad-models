/* [Tape] */
// Tape Width
TW = 16.3;
// Tape Length
TL = 88.0;
// Hole center position height
HC = 29.0;
// Hole center position distance to side
HS = 19.5;
// Hole height
HH = 8.0;
// Hole width
HW = 4.0;

/* [Case] */
// Number of slots
NR = 4;
// Inner wall width
WI = 3.0;
// Outer wall width
WO = 2.0;

/* [Stuff] */
$fa = 5;
$fs = 0.2;
e = 0.01;

// %translate([-2,90,-3]) rotate([90,0,0]) import("Tape_Holder_for_TZe_Tape.STL");

module tape(w=16.3){
    difference() {
        rotate([90,0,90]) linear_extrude(w) {
            polygon([
                    [0, 7.5],
                    [0, 67.5],
                    [TL, 67.5],
                    [TL, 0],
                    [TL-27+4, 0],
                    [TL-27, 2],
                    [TL-27-4, 0],
                    [31, 0],
                    [29, 3],
                    [12, 5],
                    [8, 5],
                    [7, 7.5],
            ]);
        }
        for (yt=[0,HW+5]) translate([0, TL-HS+HW/2+yt, HC-HH/2]) rotate([90,0,0]) linear_extrude(HW) polygon([
                [-e,  0],
                [-e, HH],
                [1, HH-2],
                [1,   2],
        ]);
    }
    difference(){
        translate([-WI-e,   TL-HS-(HW+1)/2, HC+HH/2-20])   cube([WI+2*e,   2*HW+5+1, 20+e]);
        translate([-(WI-1), TL-HS-HW/2,     HC+HH/2-20-e]) cube([WI-1+2*e, 2*HW+5,   20+3*e]);
    }
}
//!tape();

module casebase(){
    difference() {
        w = NR*(TW+WI) - WI;
        h = HC + HH/2;
        translate([-WO, -WO, -WO]) cube([2*WO + w, 2*WO + TL, WO + h]);
        d = TL-HS-HW/2-1;
        translate([-WO-e, d/2, h+d/3]) rotate([0,90,0]) cylinder(d=d, h=w+2*(WO+e));
    }
}
// casebase();

// 22.8
difference() {
    casebase();
    for (i = [0:NR-1]) {
        translate([i*(TW+WI), 0, 0]) tape(TW);
    }
}
