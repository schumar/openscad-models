// What to render
render = "flatbox_bottom"; // [deckholder_bottom, deckholder_middle, deckholder_top, flatbox_bottom, flatbox_middle, flatbox_top]

/* [Box] */
// box size (inner)
bw = 196;
// box height (inner)
bh = 45;
// box paper strength
bs = 1.5;

/* [Decks] */
// card width
cw = 63;
// card height
ch = 90;
// deck tickness
ct = 12;
// deck holder corner size
dc = 10;
// deck holder support width
ds = 5;
// deck holder height
dh = bh/3;
// deck grid size
dg = 19;

/* [Generic] */
// wall strength
w = 2.0;
// nub height
nh = 2.5;
// epsilon
e = 0.01;
// maximum segment size
$fs = 0.5;
// maximum segment angle
$fa = 5;

/* [Flat Box] */
// flat box width
fw = bw - (w+cw+w);
// number of compartments
fn = 3;
// grid size
fg = 10;

module box() {
    difference(){
        translate([-bs, -bs, -bs]) cube([bs+bw+bs, bs+bw+bs, bs+bh]);
        cube([bw, bw, bh+e]);
    }
}
%box();

module nub(r) {
    intersection() {
        cylinder(r1=r, r2=0.5, h=nh);
        cube([r+e,r+e,nh+e]);
    }
}

module decknubs() {
    translate([-e,               -e,        -e])                   nub(1.4*w);
    translate([w+cw+w+e,         -e,        -e]) rotate([0,0, 90]) nub(1.4*w);
    translate([w+cw+w+e, w+ch+w/2,          -e]) rotate([0,0,180]) nub(1.1*w);
    translate([w+cw+w+e, w+ch+w/2-e,        -e]) rotate([0,0, 90]) nub(1.1*w);
    translate([-e,       w+ch+w/2,          -e]) rotate([0,0,270]) nub(1.1*w);
    translate([-e,       w+ch+w/2-e,        -e])                   nub(1.1*w);
}

module boxnubs() {
    translate([-e,               -e,        -e])                   nub(1.4*w);
    translate([fw+e,         -e,        -e]) rotate([0,0, 90]) nub(1.4*w);
    translate([fw+e, bw/2+e,          -e]) rotate([0,0,180]) nub(1.4*w);
    translate([-e,       bw/2+e,          -e]) rotate([0,0,270]) nub(1.4*w);
}

module deckholder(bottom=true, top=false) {
    difference() {
        // big box
        cube([w+cw+w, bw/2, dh]);
        // deck
        translate([w, w, w]) cube([cw, ch, dh]);
        // back cutout
        translate([w+dc, -e, w]) cube([cw-2*dc, w+2*e, dh]);
        // side cutouts
        translate([-e, w+dc, w]) cube([cw+2*w+2*e, ch-2*dc, dh]);
        // remove useless front
        translate([w, w+ch+w, -e]) cube([cw, bw/2-ch, dh+2*e]);
        // front pickup slot
        translate([w+    dc+ds,  w+ch+w, -e]) cylinder(r=ds, h=dh+2*e);
        translate([w+cw-(dc+ds), w+ch+w, -e]) cylinder(r=ds, h=dh+2*e);
        translate([w+dc+ds, w+ch+w-ds, -e]) cube([cw-2*(dc+ds), ds+e, dh+2*e]);

        // math stuff for hexagons -- https://en.wikipedia.org/wiki/Hexagon
        ri = (dg-w)/2;                // radius of inscribed circle of hole
        R  = ri/sqrt(3)*2;            // circumradius of hole
        h  = 1.5 * dg/2/sqrt(3)*2;    // height of each row (with wall)
        nx = ceil(cw/2 / dg);         // how many holes per row (-nx:nx)
        ny = ceil(ch/2 / h);          // how many holes per column (-ny:ny)

        intersection() {
            translate([w+ds, w+ds, -e]) cube([cw-2*ds, ch-3*ds, w+2*e]);
            for (x=[-nx:nx]) for (y=[-ny:ny]) {
                translate([w+cw/2+x*dg + (y % 2 ? dg/2 : 0),  w+ch/2-ds/2+y*h, -e])
                    rotate([0,0,30]) cylinder($fn=6, r=R, h=w+2*e);
            }
        }

        // little nubs at bottom
        if (! bottom) { decknubs(); }
    }

    // nubs at top
    if (! top) {
        translate([0, 0, dh]) decknubs();
    }

    %translate([w,w,w]) cube([cw,ch,ct]);

}
if (render == "deckholder_bottom") {
    deckholder(bottom=true, top=false);
} else if (render == "deckholder_middle") {
    deckholder(bottom=false, top=false);
} else if (render == "deckholder_top") {
    deckholder(bottom=false, top=true);
} else {
    %deckholder(bottom=true, top=true);
}

module flatbox(bottom=true, top=false) {
    // compartment width
    cow = (fw - (fn+1)*w) / fn;
    echo(cow=cow);

    difference() {
        cube([fw, bw/2, dh]);
        for (n=[0:fn-1]) translate([w+n*(cow+w), w, -e]) cube([cow, bw/2-2*w, dh+2*e]);
        if ( ! bottom ) { boxnubs(); }
    }

    difference() {
        cube([fw, bw/2, w]);
        // math stuff for hexagons -- https://en.wikipedia.org/wiki/Hexagon
        ri = (fg-w)/2;                // radius of inscribed circle of hole
        R  = ri/sqrt(3)*2;            // circumradius of hole
        h  = 1.5 * fg/2/sqrt(3)*2;    // height of each row (with wall)
        nx = ceil(fw/2 / fg);         // how many holes per row (-nx:nx)
        ny = ceil(bw/4 / h);          // how many holes per column (-ny:ny)

        for (x=[-nx:nx]) for (y=[-ny:ny]) {
            translate([fw/2+x*fg + (y % 2 ? fg/2 : 0),  bw/4+y*h, -e])
                rotate([0,0,30]) cylinder($fn=6, r=R, h=w+3*e);
        }

        if ( ! bottom ) { boxnubs(); }
    }

    if ( ! top ) { translate([0,0,dh]) boxnubs(); }
}
translate([w+cw+w,0,0])
    if (render == "flatbox_bottom") {
        flatbox(bottom=true, top=false);
    } else if (render == "flatbox_middle") {
        flatbox(bottom=false, top=false);
    } else if (render == "flatbox_top") {
        flatbox(bottom=false, top=true);
    } else {
        %flatbox(bottom=true, top=true);
    }

