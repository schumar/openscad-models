/*
 * Aperiodic Monotile (aka "einstein")
 * https://arxiv.org/pdf/2303.10798.pdf
 */
base = 10;  // base length
thick = 2;  // thickness

s3 = sqrt(3);

module tile() linear_extrude(1) scale([1,s3]) polygon([
        [-2,   +0],
        [-2,   +1],
        [-1,   +1],
        [-0.5, +1.5],
        [+1,   +1],
        [+1,   +0],
        [+2,   +0],
        [+2.5, -0.5],
        [+1,   -1],
        [-0.5, -0.5],
        [-1,   -1],
        [-3,   -1],
        [-3.5, -0.5],
]);

scale([base,base,thick]) tile();
scale([base,base,thick]) %translate([2,2*s3,0]) rotate([0,0,60]) tile();
