// Durchmesser Flaeschchen
d = 30.5;
// Hoehendifferenze
h = 37;
// Anzahl Flaschen (vorne)
n = 3; // [1:10]
// Lochtiefe
l = 6;

// Verbinder links
vl = true;
// Verbinder rechts
vr = true;

// Wandstaerke
w = 0.4*2+0.5+0.05;
// Bodenstaerke
b = 1.2;

// Tiefenunterschied
t = d * sin(60);

// Epsilon
e = 0.05;

$fs = 0.4;
$fa = 10;

module flask() {
    cylinder(d=d+e, h=50, $fa=5);
    translate([0,0,50-e]) cylinder(d=20, h=15+e);
    translate([0,0,65-e]) cylinder(d1=20, d2=9, h=10);
}

module conn() {
    polygon([ [0,-3], [0, 3], [3, 3], [3, 6], [6, 6], [6,-6], [3,-6], [3, -3]]);
}

difference() {
    union() {
        cube([n*d, d+t+2*w, b+l]);
        translate([0, d/2+w, 0]) cube([n*d, d/2+t+w, b+h+l]);
        translate([0, d/2+w+t, 0]) cube([n*d, d/2+w, b+h+l+30]);
        if (vl)
            translate([e, w+d/2+t, 0]) linear_extrude(b+h) mirror([1,0]) conn();
    }
    for (i=[1:n])
        translate([i*d - d/2, d/2+w, b]) #flask();
    for (i=[0:n])
        translate([i*d, d/2+t+w, b+h]) #flask();
    if (vl)
        #translate([n*d+e, w+d/2+t, -e/2]) linear_extrude(w+h+e) mirror([1,0]) conn();

}

