// Breite Holz
b = 20;

// Wandstaerke
w = 2.5;

// Drahtdurchmesser
d = 2.8;  // 2.5mm2: 1.78mm, 6mm2: 2.76mm

// Ohrenlaenge
o = 40;

e = 1e-3;

zctr = (20-2*w)/2;

$fs = 0.3;
$fa = 5;

// base plate
translate([0, 0, -w]) linear_extrude(w) {
    difference(){
        // base plate
        polygon([
                [-b/2, -30], [-b/2, 0],
                [-b/2-10, 10], [-b/2-o, 10], [-b/2-o, 20],
                [-b/2-10, 20], [-b/2, 30], [b/2, 30], [b/2+15, 20],
                [b/2+5+o, 20], [b/2+5+o, 10],
                [b/2+15, 10], [b/2, 0], [b/2, -30]
        ]);

        // screw holes
        for (y=[-25, 25]) for (x=[-1,1])
            translate([x*(b/2-5), y]) circle(d=2);
        // translate([2.5, 15]) circle(d=2);
    }
    // ears
    translate([-b/2-o,   15]) circle(r=5);
    translate([ b/2+5+o, 15]) circle(r=5);
}


// plate with hole foar coax connector
translate([0, w, 0]) rotate([90,0,0]) linear_extrude(w) difference(){
    translate([-b/2, 0]) square([b, 20-2*w], center=false);

    translate([0, zctr])
        intersection(){
            circle(d=9.5);
            square([100, 8], center=true);
        }
}


// side triangles
module side(x){
    translate([x, 0, 0]) rotate([90,0,90]) linear_extrude(w) polygon([
            [-20, 0], [0, 20-2*w], [0, 0]
    ]);
}

side(-b/2);
side(b/2-w);


// wire holders
module wire_holder(){
    rotate([90,0,90]) linear_extrude(5) {
        translate([-5,0]) square([10,zctr]);
        translate([0,zctr]) circle(r=5);
    }
}

difference(){
    for (x=[-b/2-o, -10, 10, b/2+o])
        translate([x, 15, 0]) wire_holder();

    // left wire (to center tap)
    #translate([0-d, 15, zctr]) rotate([0, -90, 0]) cylinder(d=d, h=500);
    #translate([0-d, 15-d, zctr]) rotate_extrude(angle=90) translate([d,0]) circle(d=d);
    #translate([0,   12, zctr]) rotate([-90,0,0]) cylinder(d=d, h=15-12-d);

    // right wire (to shield)
    #translate([5+d, 15, zctr]) rotate([0, 90, 0]) cylinder(d=d, h=500);
    #translate([5+d, 15-d, zctr]) rotate([0,0,90]) rotate_extrude(angle=90) translate([d,0]) circle(d=d);
    #translate([5, 10, zctr]) rotate([-90,0,0]) cylinder(d=d, h=15-10-d);
}


// stabilizers
translate([-b/2-o+5, 15-d/2, 0]) cube([o-15+b/2, d, zctr-d]);
translate([ 15,      15-d/2, 0]) cube([o-15+b/2, d, zctr-d]);
translate([ -5,   20-d  , 0])   cube([ 15, d, zctr-d]);


// diagram of coax connector
%translate([0,  -12, zctr])  rotate([-90, 0, 0]) cylinder(d=9, h=20);
%translate([0,  -1.3, zctr]) rotate([-90, 0, 0]) cylinder(d=12, h=w+4);
%translate([0,   0, zctr])   rotate([-90, 0, 0]) cylinder(d=4, h=10);
%translate([0,   0, zctr])   rotate([-90, 0, 0]) cylinder(d=2, h=12);
%translate([4.5, 0, zctr-1]) cube([0.5, 10, 2]);

// diagram of wood
%translate([-b/2, -200+30, -w-15]) cube([b, 200, 15]);

